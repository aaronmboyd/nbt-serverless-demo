
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

const tableName = process.env.FRIDGR_TABLE_NAME;
const defaultHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
}

module.exports.create = (fridgrObj) => {

    let params = {
        TableName: tableName,
        Item: fridgrObj
    };
    return documentClient.put(params).promise();
};

module.exports.createHttpResponse = (status, body) => {
    return {
        statusCode: status,
        isBase64Encoded: false,
        headers: defaultHeaders,
        body: JSON.stringify(body)
    };
}

module.exports.get = (fridgeId) => {
    let params = {
        TableName: tableName,
        Key: {
            fridgeId: `${fridgeId}`
        }
    };
    return documentClient.get(params).promise();
};

module.exports.getAll = () => {
    let params = {
        TableName: tableName
    };
    return documentClient.scan(params).promise();
};