
##### Prerequisities #######

# node 6+ at least
node --version
npm --version

# aws-cli installed?
aws --version

# framework iam user configured?
aws iam list-users

# if not - configure a new user
aws configure --profile nbt-serverless-demo
export AWS_PROFILE=nbt-serverless-demo
aws iam list-users

# install serverless framework
npm install -g serverless

##### Setup #####
mkdir nbt-serverless-demo
git init
serverless create --template aws-nodejs

# Update provider: tag
# provider:
#   name: aws
#   runtime: nodejs10.x
#   profile: nbt-serverless-demo
#   stage: demo
#   region: eu-central-1

# Update Lambda event trigger
# functions:
#   hello:
#     handler: handler.hello
#     events:
#         - http:
#             path: hello
#             method: get
#             cors: true

##### Deploy! #####
serverless deploy -v --stage demo

# Check the CloudFormation creation (how cool is drift detection!?)
# See API Gateway created
# See Lambda created

# Test function locally
sls invoke local --function hello

# Test remotely
curl https://990wk3n5g4.execute-api.eu-central-1.amazonaws.com/demo/hello

#### Make it do something, add some DynamoDB resources #####

#### Create a document DB called ... say FridgrTable
resources:
  Resources:
    FridgrTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:service}-${self:provider.stage}-fridgr
        AttributeDefinitions:
          - AttributeName: fridgeId
            AttributeType: S
        KeySchema:
          - AttributeName: fridgeId
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1

# Make sure the Lambda has perms to do things to it
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:PutItem
        - dynamodb:GetItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource:
        - Fn::Join:
          - ':'
          -
            - 'arn:aws:dynamodb:${self:provider.region}'
            - Ref: 'AWS::AccountId'
            - 'table/${self:service}-${self:provider.stage}-*'

# Store it as an environment variable under provider
  environment:
    FRIDGR_TABLE_NAME: ${self:service}-${self:provider.stage}-fridgr

# Deploy this, see if anything is created in CloudFormation?
# Still working?
sls invoke local --function hello

##### Save a fridgr to DynamoDB ####

### 1. Create a new path
  createFridgR:
    handler: fridgr.handler
    events:
        - http:
            path: fridgr
            method: post
            cors: true

#### 2. Create simple util file to interact with DynamoDB and some helpers, util.js

const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

const tableName = process.env.FRIDGR_TABLE_NAME;
const defaultHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
}

module.exports.create = (fridgrObj) => {

    let params = {
        TableName: tableName,
        Item: fridgrObj
    };
    return documentClient.put(params).promise();
};

module.exports.createHttpResponse = (status, body) => {
    return {
        statusCode: status,
        isBase64Encoded: false,
        headers: defaultHeaders,
        body: JSON.stringify(body)
    };
}

#### 3. Create a new file fridgr.js to post to DB
const utils = require('./utils');
const uuid = require('uuid/v4');

module.exports.handler = async (event, context, callback) => {

  const fridgrObj = event.body && JSON.parse(event.body).fridgr;

  if (!fridgrObj) {
    callback(null, utils.createHttpResponse(400, {message: 'Bad request - No fridge passed'}));
  }
  else{
    fridgrObj["fridgeId"] = uuid();
    try{        
        const result = await utils.create(fridgrObj);
        const body = {fridgr: fridgrObj};
        console.log('Cool starry bro');
        callback(null, utils.createHttpResponse(201, body));
    }
    catch(error){
        console.log('Error promise resolved');
        callback(null, utils.createHttpResponse(500, {message: 'Server error during creation of fridgr - ' + error }));
    }  
  }
};

#### 4. Deploy and test!
sls deploy -v --stage demo

# We should now have a new endpoint!
# Test in Postman for ease

## Error!?
## Let's check Cloud logs
    "errorType": "Runtime.ImportModuleError",
    "errorMessage": "Error: Cannot find module './utils'",

# Fix, redeploy!
    "errorType": "Runtime.ImportModuleError",
    "errorMessage": "Error: Cannot find module 'uuid/v4'",

# Why not? Because we didn't initialise a node project and include it as a dependancy
yarn init
yarn add uuid/v4

#### What about retrieval?

# 1. Add this snippet to utils
module.exports.get = (fridgeId) => {
    let params = {
        TableName: tableName,
        Key: {
            fridgeId: `${fridgeId}`
        }
    };
    return documentClient.get(params).promise();
};

module.exports.getAll = () => {
    let params = {
        TableName: tableName
    };
    return documentClient.scan(params).promise();
};

# 2. Create file fridgrGet.js
const utils = require('./util');

module.exports.handler = async (event, context, callback) => {

  const fridgeIdPassed = event.queryStringParameters ? (event.queryStringParameters.fridgeId ? true : false) : false;
  console.log("Query parameters are " + JSON.stringify(event.queryStringParameters));    

  try{
    if(fridgeIdPassed){
        const result = await utils.get(event.queryStringParameters.fridgeId);
        const body = {fridgr: result };
        callback(null, utils.createHttpResponse(200, body)); 
      }
      else{
        const result = await utils.getAll()
        const fridgrs = result && result.Items || [];
        const body = {fridgrs: fridgrs };
        callback(null, utils.createHttpResponse(200, body));
      }    
  }
  catch(error){
    console.log('Error promise resolved');
    callback(null, utils.createHttpResponse(500, {message: 'Server error during retrieval of fridgr - ' + error }));
  }
}

## 3. Create new path
  getFridgR:
    handler: fridgrGet.handler
    events:
        - http:
            path: fridgr
            method: get
            cors: true

## 4. Deploy and test!
sls deploy -v --stage demo

## 5. Danke!
### Conclusion: Serverless framework is incredibly fast to spin up, very cheap, and a good API solution even at scale
### If it becomes a problem... it's a great problem to have...

Free Tier - 1M API CALLS RECEIVED | 1M MESSAGES | 750,000 CONNECTION MINUTES
https://aws.amazon.com/api-gateway/pricing/

Free Tier - 1M free requests per month and 400,000 GB-seconds of compute time per month.
https://aws.amazon.com/lambda/pricing/















