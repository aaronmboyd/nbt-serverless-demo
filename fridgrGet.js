
const utils = require('./util');

module.exports.handler = async (event, context, callback) => {

  const fridgeIdPassed = event.queryStringParameters ? (event.queryStringParameters.fridgeId ? true : false) : false;
  console.log("Query parameters are " + JSON.stringify(event.queryStringParameters));    

  try{
    if(fridgeIdPassed){
        const result = await utils.get(event.queryStringParameters.fridgeId);
        const body = {fridgr: result };
        callback(null, utils.createHttpResponse(200, body)); 
      }
      else{
        const result = await utils.getAll()
        const fridgrs = result && result.Items || [];
        const body = {fridgrs: fridgrs };
        callback(null, utils.createHttpResponse(200, body));
      }    
  }
  catch(error){
    console.log('Error promise resolved');
    callback(null, utils.createHttpResponse(500, {message: 'Server error during retrieval of fridgr - ' + error }));
  }
}