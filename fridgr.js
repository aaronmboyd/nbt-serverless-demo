const utils = require('./util');
const uuid = require('uuid/v4');

module.exports.handler = async (event, context, callback) => {

  const fridgrObj = event.body && JSON.parse(event.body).fridgr;

  if (!fridgrObj) {
    callback(null, utils.createHttpResponse(400, {message: 'Bad request - No fridge passed'}));
  }
  else{
    fridgrObj["fridgeId"] = uuid();
    try{        
        const result = await utils.create(fridgrObj);
        const body = {fridgr: fridgrObj};
        console.log('Cool starry bro');
        callback(null, utils.createHttpResponse(201, body));
    }
    catch(error){
        console.log('Error promise resolved');
        callback(null, utils.createHttpResponse(500, {message: 'Server error during creation of fridgr - ' + error }));
    }  
  }
};